package aeronet

import (
	"bufio"
	"bytes"
	"encoding/csv"
	//"fmt"
	"io"
	"log"
	"os"
	"strconv"

	"gonum.org/v1/gonum/mat"
)

//go:generate stringer -type=AeronetColumn
type AeronetColumn int

const (
	AOD_EXT_440nm AeronetColumn = iota
	AOD_EXT_675nm
	AOD_EXT_870nm
	AOD_EXT_1020nm
	ANGSTROM_EXT
	AOD_ABS_440nm
	AOD_ABS_675nm
	AOD_ABS_870nm
	AOD_ABS_1020nm
	ANGSTROM_ABS
	MRE_440nm
	MRE_675nm
	MRE_870nm
	MRE_1020nm
	MIM_440nm
	MIM_675nm
	MIM_870nm
	MIM_1020nm
	SPHERICITY_FACTOR
	R1
	R2
	R3
	R4
	R5
	R6
	R7
	R8
	R9
	R10
	R11
	R12
	R13
	R14
	R15
	R16
	R17
	R18
	R19
	R20
	R21
	R22
	VOLC_T
	REFF_T
	VMR_T
	STD_T
	VOLC_F
	REFF_F
	VMR_F
	STD_F
	VOLC_C
	REFF_C
	VMR_C
	STD_C
	LR_440nm
	LR_675nm
	LR_870nm
	LR_1020nm
	DEP_440nm
	DEP_675nm
	DEP_870nm
	DEP_1020nm
	SALB_440nm
	SALB_675nm
	SALB_870nm
	SALB_1020nm
	NCOLS
)

// Find how many lines in text file
func LineCounter(r io.Reader) (int, error) {

	var count int
	const lineBreak = '\n'

	buf := make([]byte, bufio.MaxScanTokenSize)

	for {
		bufferSize, err := r.Read(buf)
		if err != nil && err != io.EOF {
			return 0, err
		}

		var buffPosition int
		for {
			i := bytes.IndexByte(buf[buffPosition:], lineBreak)
			if i == -1 || bufferSize == buffPosition {
				break
			}
			buffPosition += i + 1
			count++
		}
		if err == io.EOF {
			break
		}
	}

	return count, nil
}

// Читает файл Aeronet из которого предварительно удалили лишние 6 строк в
// заголовке
// TODO: изменить, чтобы подпрограмма сама удаляла лишние строки
func ReadAeronet(fname string) *mat.Dense {
	file, err := os.Open(fname)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Читаем число строк, нужно для инициализации возвращаемой мтрицы
	Nlines, err := LineCounter(bufio.NewReader(file))

	if err != nil {
		log.Fatal(err)
	}

	file.Seek(0, 0)

	// место под хранение прочитанных данных
	tbl := mat.NewDense(Nlines-1, int(NCOLS), nil)

	reader := csv.NewReader(file)
	reader.Comment = '#'
	reader.Comma = ','

	// читеаем заголовок
	_, err = reader.Read()

	for i := 0; i < Nlines-1; i++ {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		colidx := 0
		for j := 0; j < 4; j++ {
			tmp, _ := strconv.ParseFloat(record[10+j], 64)
			tbl.Set(i, colidx, tmp)
			colidx++
		}

		tmp, _ := strconv.ParseFloat(record[22], 64)
		tbl.Set(i, colidx, tmp)
		colidx++

		for j := 0; j < 13; j++ {
			tmp, _ := strconv.ParseFloat(record[27+j], 64)
			tbl.Set(i, colidx, tmp)
			colidx++
		}

		for j := 0; j < 23; j++ {
			tmp, _ := strconv.ParseFloat(record[52+j], 64)
			tbl.Set(i, colidx, tmp)
			colidx++
		}

		for j := 0; j < 12; j++ {
			tmp, _ := strconv.ParseFloat(record[76+j], 64)
			tbl.Set(i, colidx, tmp)
			colidx++
		}

		for j := 0; j < 8; j++ {
			tmp, _ := strconv.ParseFloat(record[112+j], 64)
			tbl.Set(i, colidx, tmp)
			colidx++
		}

		for j := 0; j < 4; j++ {
			tmp, _ := strconv.ParseFloat(record[141+j], 64)
			tbl.Set(i, colidx, tmp)
			colidx++
		}
		//fmt.Print(colidx)
	}

	return tbl
}
