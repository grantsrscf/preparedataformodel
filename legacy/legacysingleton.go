package legacy

import (
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"reflect"
	"unsafe"

	"gonum.org/v1/gonum/mat"
)

//#cgo LDFLAGS: -L -lgfortran -L ../lib -lspheroid
//#cgo CFLAGS: -I ../include/
//#include "legacy.h"
//#include <stdlib.h>
import "C"

// Отображает массив типа float из  С на slice в  Го
// при этом м слайс и массив использут один и тот же кусок памяти
func carray2slice(array *C.float, len int) []float32 {
	var list []float32

	sliceHeader := (*reflect.SliceHeader)((unsafe.Pointer(&list)))
	sliceHeader.Cap = len
	sliceHeader.Len = len
	sliceHeader.Data = uintptr(unsafe.Pointer(array))
	return list
}

// Преобразует слайс типа float32 в слайт типа float64
// здесь выполняется копирование с приведением типа кажждого элемента к float64
func float32tofloat64(array []float32) []float64 {
	N := len(array)
	list := make([]float64, N)

	for i := 0; i < N; i++ {
		list[i] = float64(array[i])
	}

	return list
}

type singleton struct {
}

var instance *singleton = nil

//var once sync.Once

type LNmode struct {
	Rmm float64
	Sm  float64
	Cm  float64
}

// Объявление интерфейсов

// доступ к результатам расчетов
type Result interface {
	Angle() []float64
	F11() []float64
	F12() []float64
	F22() []float64
	F33() []float64
	F34() []float64
	F44() []float64
	MuellerMatrix() ([]float64, *mat.Dense)
	Xext() float64
	Xsca() float64
	Xabs() float64
	Ssa() float64
	Lr() float64
	Ldr() float64
	AcAeronet() float64
	Ac0() float64
}

//
type InputPars interface {
}

// доступ к фуннкциям вычислений
type Operational interface {
	//ReadInitFile()
	ReadConfigFile(fname string)
	ReadConfig(fin io.Reader)
	SetDistnameO(fname string)
	SetDistnameF(fname string)
	SetDistnameN(fname string)
	SetCommName(id int, data string)
	AllocateMemory()
	DeallocateMemory()
	DoCalc(ndp int) int
	SaveResult(prefix string, idx int)
}

// Интерфейс, который реализует тип singleton
type Singleton interface {
	Result
	Operational

	RatiosX() []float64
	RatiosY() []float64
	GetSd() []float64
	GetGrid() []float64
	SetPSD(x, y []float64)
	SetSd(y []float64)

	GetRrr() []float64
	GetAr() []float64
	GetXgrid() []float64

	Ac() float64

	GetCm() []float64
	GetSm() []float64
	GetRmm() []float64

	GetLNmode() []LNmode
	SetLNmode(mode []LNmode)

	SetLNPars(cm, sm, rmm []float64)

	GetRn() float64
	SetRn(rn float64)
	GetRk() float64
	SetRk(rk float64)
	GetWl() float64
	SetWl(wl float64)

	PrintKeyParams()
}

// возвращает сущность объекта, который создается один раз
// и существует в программе в единственном экземпляре
func GetInstance() Singleton {

	if instance == nil {
		instance = new(singleton)
	}
	// once.Do(func() {
	// 	instance = new(singleton)
	// })
	return instance
}

/// реализация интерфейса

// чтение файла с настройками input.dat
// Вызывает соответствующую процедуру из dll файла
//func (s *singleton) ReadInitFile() {
//	C.dls_read_input_()
//}

func (s *singleton) ReadConfigFile(fname string) {
	cfname := C.CString(fname)
	N := len(fname)
	cN := C.int(N)
	defer C.free(unsafe.Pointer(cfname))
	C.dls_read_config_(cfname, &cN)
}

func (s *singleton) SetDistnameO(fname string) {
	cfname := C.CString(fname)
	defer C.free(unsafe.Pointer(cfname))
	C.set_distname_o(cfname)
}

func (s *singleton) SetDistnameF(fname string) {
	cfname := C.CString(fname)
	defer C.free(unsafe.Pointer(cfname))
	C.set_distname_f(cfname)
}

func (s *singleton) SetDistnameN(fname string) {
	cfname := C.CString(fname)
	defer C.free(unsafe.Pointer(cfname))
	C.set_distname_n(cfname)
}

func (s *singleton) SetCommName(id int, data string) {
	cdata := C.CString(data)
	defer C.free(unsafe.Pointer(cdata))
	tmp := C.int(id)
	C.set_comm_name(&tmp, cdata)
}

// выделение памяти под внутренние переменные в куче
// вызывать строго после ReadInitFile
func (s *singleton) AllocateMemory() {
	KA := C.int(1)
	C.alloc_dls_array_(&C.key,
		&C.keyel,
		&KA)
}

// освобождение памяти вынутренних объектов
// вызывать после AllocateMemory в конце программы
func (s *singleton) DeallocateMemory() {
	KA := C.int(2)
	C.alloc_dls_array_(&C.key,
		&C.keyel,
		&KA)
}

// Perform calculations
// if ndp = 0, loads database from disk before
// else if ndp=1, then data already loaded into memory
func (s *singleton) DoCalc(ndp int) int {
	C.ndp = C.int(ndp)
	if ndp == 0 {
		log.Println("Loading kernels from disk")
	}
	C.optchar_(&C.ndp)
	return int(C.ndp)
}

/// GetRatiosX - возвращает отношение полуосей элипсоида
func (s *singleton) RatiosX() []float64 {
	vect := carray2slice(&C.r[0], int(C.kr))
	return float32tofloat64(vect)
}

/// GetRatiosY - возвращает относительный вклад в концентрацию
func (s *singleton) RatiosY() []float64 {
	vect := carray2slice(&C.rd[0], int(C.kr))
	return float32tofloat64(vect)
}

/// GetSd - значения ординат функции распределения, заданные таблично
func (s *singleton) GetSd() []float64 {
	vect := carray2slice(&C.sd[0], int(C.kn))
	return float32tofloat64(vect)
}

/// GetGrid - значение абсцисс функции распределения, заданные таблично
func (s *singleton) GetGrid() []float64 {
	vect := carray2slice(&C.grid[0], int(C.kn))
	return float32tofloat64(vect)
}

func (s *singleton) SetPSD(x, y []float64) {
	N := len(x)
	for i := 0; i < N; i++ {
		C.grid[i] = C.float(x[i])
		C.sd[i] = C.float(y[i])
	}

	C.kn = C.int(N)
}

func (s *singleton) SetSd(y []float64) {
	N := len(y)
	for i, v := range y {
		C.sd[i] = C.float(v)
	}
	C.kn = C.int(N)
}

/// Значение абсцисс функции распределения, в случае если
/// функция задается как логнормальное распределение
func (s *singleton) GetRrr() []float64 {
	vect := carray2slice(&C.rrr[0], int(C.kn))
	return float32tofloat64(vect)
}

/// Значение ординат функции распределения, в случае если
/// функция задается как логнормальное распределение
func (s *singleton) GetAr() []float64 {
	vect := carray2slice(&C.ar[0], int(C.kn))
	return float32tofloat64(vect)
}

/// Значение отсчетов, похоже это значения, на что интерполируется Sd
func (f *singleton) GetXgrid() []float64 {
	vect := carray2slice(&C.xgrid[0], int(C.kn))
	return float32tofloat64(vect)
}

/// возвращает вектор углов рассеяния, для которых выполнялись расчеты
/// матрицы
func (f *singleton) Angle() []float64 {
	vect := carray2slice(&C.angle[0], int(C.km))
	return float32tofloat64(vect)
}

/**
F11, F12, F22, F33, F44, F34 - ненулевые элементы матрицы рассеяния,
нормированные на F11
*/
func (f *singleton) F11() []float64 {
	vect := carray2slice(&C.f11[0], int(C.km))
	return float32tofloat64(vect)
}

func (f *singleton) F12() []float64 {
	vect := carray2slice(&C.f12[0], int(C.km))
	return float32tofloat64(vect)
}

func (f *singleton) F22() []float64 {
	vect := carray2slice(&C.f22[0], int(C.km))
	return float32tofloat64(vect)
}

func (f *singleton) F33() []float64 {
	vect := carray2slice(&C.f33[0], int(C.km))
	return float32tofloat64(vect)
}

func (f *singleton) F34() []float64 {
	vect := carray2slice(&C.f34[0], int(C.km))
	return float32tofloat64(vect)
}

func (f *singleton) F44() []float64 {
	vect := carray2slice(&C.f44[0], int(C.km))
	return float32tofloat64(vect)
}

// В случае, если функция распределения задавалась при помощи формул
// вернет значение концентрации
func (f *singleton) Ac() float64 {
	return float64(C.ac)
}

// В случае, если функция распределения задавалась таблично и отсчеты
// соответствуют отсчетам в файле aeronet, то функция вернет значение
// объемной концентрации
func (f *singleton) AcAeronet() float64 {
	dlnr := 0.2716
	ret := 0.0
	y := f.GetSd()

	for _, yi := range y {
		ret += yi
	}
	ret *= dlnr
	return ret
}

// Вычисляет объемную концентрацию
// требуется, чтобы таблично заданная функция была вида dV/dlnr
func (f *singleton) Ac0() float64 {
	x := f.GetGrid()
	y := f.GetSd()
	N := len(x)
	ret := float64(0.0)

	for i := 1; i < N; i++ {
		dlnr := math.Log(x[i]) - math.Log(x[i-1])
		Ym := 0.5 * (y[i] + y[i-1])
		ret += Ym * dlnr
	}
	return ret
}

// возвращает стлайс с концентрациями для каждой из используемых мод
func (f *singleton) GetCm() []float64 {
	vect := carray2slice(&C.cm[0], int(C.nmd))
	return float32tofloat64(vect)
}

// возвращает стлайс с полуширинами для каждой из используемых мод
func (f *singleton) GetSm() []float64 {
	vect := carray2slice(&C.sm[0], int(C.nmd))
	return float32tofloat64(vect)
}

// возвращает стлайс с медианными радиусами для каждой из используемых мод
func (f *singleton) GetRmm() []float64 {
	vect := carray2slice(&C.rmm[0], int(C.nmd))
	return float32tofloat64(vect)
}

// возвращает стлайс структур, описывающих  каждую из используемых мод
func (f *singleton) GetLNmode() []LNmode {
	N := int(C.nmd)
	ret := make([]LNmode, N)
	for i := 0; i < N; i++ {
		ret[i].Cm = float64(C.cm[i])
		ret[i].Sm = float64(C.sm[i])
		ret[i].Rmm = float64(C.rmm[i])
	}
	return ret
}

// устанавливает параметры функции распределения в виде комбинаци
// из логнормальных распределений
func (f *singleton) SetLNmode(mode []LNmode) {
	N := len(mode)
	if N > 2 {
		N = 2
	}
	C.nmd = C.int(N)

	for i := 0; i < N; i++ {
		C.rmm[i] = C.float(mode[i].Rmm)
		C.sm[i] = C.float(mode[i].Sm)
		C.cm[i] = C.float(mode[i].Cm)
	}
}

// аналогичная функция, но на вход подаются слайцы таждого из параметров
// распределений
func (f *singleton) SetLNPars(cm, sm, rmm []float64) {
	N := len(cm)
	if N > 2 {
		N = 2
	}

	for i := 0; i < N; i++ {
		C.cm[i] = C.float(cm[i])
		C.sm[i] = C.float(cm[i])
		C.rmm[i] = C.float(rmm[i])
	}

	C.nmd = C.int(N)
}

/// Extinction crosssection mkm^2
func (f *singleton) Xext() float64 {
	return float64(C.xext)
}

/// Absorbption crossection mkm^2
func (f *singleton) Xabs() float64 {
	return float64(C.xabs)
}

/// Scattering crosssection mkm^2
func (f *singleton) Xsca() float64 {
	return float64(C.xsca)
}

///Single scattering albedo
func (f *singleton) Ssa() float64 {
	return float64(C.albedo)
}

// Lidar Ratio
func (f *singleton) Lr() float64 {
	return float64(C.xblr)
}

/// Linear depolarization ratio
func (f *singleton) Ldr() float64 {
	return float64(C.xldr)
}

// Возвращает длину волны мкм
func (f *singleton) GetWl() float64 {
	return float64(C.wl)
}

// Устанавливает длину волны в мкм
func (f *singleton) SetWl(wl float64) {
	C.wl = C.float(wl)
}

// Возвращает и устанавливает действительную часть показателя преломления
func (f *singleton) GetRn() float64 {
	return float64(C.rn)
}

func (f *singleton) SetRn(rn float64) {
	C.rn = C.float(rn)
}

// Возвращает и устанавливает мнимую часть показателя преломления
func (f *singleton) GetRk() float64 {
	return float64(C.rk)
}

func (f *singleton) SetRk(rk float64) {
	C.rk = C.float(rk)
}

// вывод настроек расчета функций
func (f *singleton) PrintKeyParams() {
	fmt.Println("Key parameters:=============")
	fmt.Printf("key = %d\nkey_rd = %d\nkeyel = %d\nkeysub = %d\nkeyls = %d\nkey_org = %d\n",
		C.key, C.key_rd, C.keyel, C.keysub, C.keyls, C.key_org)
	fmt.Printf("key_fx = %d\nkey_grid1 = %d\nkey_rd1 = %d\nkn = %d\nkm = %d\nkr = %d\nnratn = %d\n\n",
		C.key_fx, C.key_grid1, C.key_rd1, C.kn, C.km, C.kr, C.nratn)

}

/// Возвращает угол рассеяния и матрицу рассеяния
func (f *singleton) MuellerMatrix() ([]float64, *mat.Dense) {
	ret := mat.NewDense(int(C.km), 16, nil)

	angle := f.Angle()
	AOT := f.Xsca()
	f11 := f.F11()
	f12 := f.F12()
	f22 := f.F22()
	f33 := f.F33()
	f34 := f.F34()
	f44 := f.F44()
	N := len(angle)
	f43 := make([]float64, N)

	// умножаем вектор на раскрываем нашу фазовую матрицу в матрицу
	// рассеяния, при этом интеграл первого элемента ее по телесному
	// углу должен быть равен АОТ
	for i := 0; i < N; i++ {
		f11[i] = f11[i] * AOT / (4 * math.Pi)
		f12[i] = f12[i] * f11[i]
		f22[i] = f22[i] * f11[i]
		f33[i] = f33[i] * f11[i]
		f34[i] = f34[i] * f11[i]
		f43[i] = -f34[i]
		f44[i] = f44[i] * f11[i]
	}

	// заполняем ненулевые столбцы матрицы Мюллера значениями
	ret.SetCol(0, f11)
	ret.SetCol(1, f12)
	ret.SetCol(4, f12)
	ret.SetCol(5, f22)
	ret.SetCol(10, f33)
	ret.SetCol(11, f34)
	ret.SetCol(14, f43)
	ret.SetCol(15, f44)

	return angle, ret
}

// сохранаяе результаты расчета в файт для дальнейшей постобработки
func (f *singleton) SaveResult(prefix string, idx int) {
	fname := fmt.Sprintf("%s_%05d.dat", prefix, idx)
	fout, err := os.Create(fname)
	if err != nil {
		log.Fatal(err)
	}
	defer fout.Close()

	angle, M := f.MuellerMatrix()
	rows, cols := M.Dims()
	Vc := f.Ac0()
	fmt.Fprintf(fout, "%9.3e\t%9.3e\t%9.3e\t%9.3e\t%9.3e\t# \n",
		f.Xext()/Vc, f.Xsca()/Vc, f.Xabs()/Vc, f.Lr(), f.Ldr())
	fmt.Fprintf(fout, "%9s\t%9s\t%9s\t%9s\t%9s\t%9s\t%9s\t%9s\t%9s\t"+
		"%9s\t%9s\t%9s\t%9s\t%9s\t%9s\t%9s\t%9s\n",
		"Angle", "S11", "S12", "S13", "S14", "S21", "S22", "S23",
		"S24", "S31", "S32", "S33", "S34", "S41", "S42", "S43",
		"S44")
	for i := 0; i < rows; i++ {
		fmt.Fprintf(fout, "%9.3f\t", angle[i])
		for j := 0; j < cols; j++ {
			fmt.Fprintf(fout, "%9.3e\t", M.At(i, j)/Vc)
		}
		fmt.Fprintf(fout, "\n")
	}
}

/// LDFLAGS: -L /usr/local/Cellar/gcc/11.2.0/lib/gcc/11 -lgfortran -L ../lib -lspheroid
