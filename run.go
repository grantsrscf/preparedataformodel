package main

import (
	"bytes"
	_ "embed"
	"flag"
	"fmt"
	"log"

	"gonum.org/v1/gonum/interp"
	//"gonum.org/v1/gonum/mat"
	"kshmirko.com/dubovik/aeronet"
	"kshmirko.com/dubovik/legacy"
)

//go:embed input_sphrds.dat
var f_spheroids []byte

//go:embed input_sphrs.dat
var f_spheres []byte

func main() {

	//configFile := flag.String("config", "input_sphrds.dat",
	//	"Имя файла конфигурации.")
	calc_spheres := flag.Bool("s", false, "Use spheres?")
	spherictyLo := flag.Float64("spher_lo", 1.0,
		"Нижняя граница сферичности")
	spherictyHi := flag.Float64("spher_hi", 99.0,
		"Верхняя граница сферичности")
	maxNrows := flag.Int("N", 5,
		"Число строк для обработки, -1 - обрабатывать все строки")
	prefixStr := flag.String("prefix", "sphrds",
		"Префикс для файла с матрицами")
	wvlPar := flag.Float64("wvl", 0.800,
		"Длина волны излучения для которой выполняется расчет")
	flag.Parse()

	filename := flag.Arg(0)

	if filename == "" {
		log.Fatal("Файл с таблицей AERONET не предоставлен")
	}

	tbl := aeronet.ReadAeronet(filename)
	nrows, _ := tbl.Dims()

	// Если запрашиваемое число строк для вывода дольше фактического
	// количесва строк, то выводим столько, сколько есть
	if (*maxNrows > 0) && (*maxNrows < nrows) {
		nrows = *maxNrows
	}

	ss := legacy.GetInstance()

	// в зависивости от наличия или остутствия флага '-s'
	// читаем тот или иной конфиг
	if *calc_spheres {
		ss.ReadConfig(bytes.NewReader(f_spheres))
	} else {
		ss.ReadConfig(bytes.NewReader(f_spheroids))
	}

	ss.AllocateMemory()

	wvl := *wvlPar

	wvls := []float64{0.440, 0.675, 0.870, 1.020}
	ss.SetWl(wvl)

	ndp := 0
	var pl interp.PiecewiseLinear

	for i := 0; i < nrows; i++ {
		row := tbl.RawRowView(i)

		// Выбираем измерения? где сферичность <= 20
		// TODO: пороговую сферичность нужно рассматривать как параметр
		if (row[aeronet.SPHERICITY_FACTOR] >= *spherictyLo) &&
			(row[aeronet.SPHERICITY_FACTOR] <= *spherictyHi) {

			mre := row[aeronet.MRE_440nm : aeronet.MRE_1020nm+1]
			mim := row[aeronet.MIM_440nm : aeronet.MIM_1020nm+1]

			// Интерполяция действительной части показателя преломления
			pl.Fit(wvls, mre)
			mr_i := pl.Predict(wvl)

			// Интерполяция мнимой части показателя преломления
			pl.Fit(wvls, mim)
			mi_i := pl.Predict(wvl)

			// извлекаем функцию распределения
			dvdlnr := row[aeronet.R1 : aeronet.R22+1]

			// настройка модели
			ss.SetSd(dvdlnr)
			ss.SetRn(mr_i)
			ss.SetRk(mi_i)
			ndp = ss.DoCalc(ndp)

			// сохраняем матрицу
			ss.SaveResult(*prefixStr, i)

			fmt.Printf("%4.1f\t", row[aeronet.SPHERICITY_FACTOR])
			fmt.Printf("%5d\t", i)
			fmt.Printf("V = %5.3f\t", ss.Ac0())
			fmt.Printf("Ext = %.3f\t", ss.Xext())
			fmt.Printf("Abs = %.3f\t", ss.Xabs())
			fmt.Printf("LR = %6.2f\t", ss.Lr())
			fmt.Printf("LDR = %5.2f\t", ss.Ldr())
			fmt.Printf("midx= (%4.2f, %5.3f)\n", mr_i, mi_i)
		}

	}

	ss.DeallocateMemory()

}
